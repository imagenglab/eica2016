### README ###
Repository for class 2016, course "Elaborazione delle Immagini per la Chirurgia Assistita".

### OUTLINE ###

Notebook_0: Introduction to Python 

Notebook_1: Introduction to image procesing in Python

Notebook_2: Fourier

Notebook_3: convolution

Notebook_4: Introduction to medical image processing in python

Notebook_5: Morphological filters

Notebook_6: Image segmentation

Notebook_7: Digitally reconstructed radiography

Notebook_8: Features

Notebook_9: Camera calibration

Notebook_10: Triangulation

Notebook_11: Marching cubes

Notebook_12: Compute rototranslation

